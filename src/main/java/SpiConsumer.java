import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@SuppressWarnings({"SpellCheckingInspection", "ArraysAsListWithZeroOrOneArgument", "InfiniteLoopStatement"})
public class SpiConsumer {
    //Connection Postgres;
    static Connection conn;


    public static void main(String[] args) {
        //consumer properties and configurations
        final Logger logger = LoggerFactory.getLogger(SpiConsumer.class);
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.41.238:9092,192.168.41.240:9092,192.168.41.241:9092");
        props.put("group.id", "SpiMonitoring");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put("client.id", "SpiMonitoring");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

        //consumer sub
        consumer.subscribe(Arrays.asList("singularcore.sis_payments.transactions"));

        try {
            //Establish connection + PrepareStatement
            conn = DriverManager.getConnection("jdbc:postgresql://192.168.41.17:5432/postgres", "postgres", "ABMon$22bix");
            PreparedStatement statement = conn.prepareStatement("INSERT INTO \"SPI\".expresspay(status,userid,currency,id,date,coretransactionid)" + "VALUES (?,?,?,?,?,?)");
            PreparedStatement statement2 = conn.prepareStatement("UPDATE \"SPI\".expresspay SET status = ?, userid = ?, currency = ?, date = ?, coretransactionid = ?  WHERE id = ?");

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(10000));
                for (ConsumerRecord<String, String> record : records) {

                    String jsonString = record.value();
                    JSONObject obj = new JSONObject(jsonString).getJSONObject("payload");
                    ObjectMapper mapper = new ObjectMapper();
                    Message message = mapper.readValue(obj.toString(), Message.class);

                    if (message.getBefore() == null && message.getAfter() != null) {
                        if (message.getAfter().getProviderLabel() != null && (message.getAfter().getProviderLabel().equals("expresspay"))) {

                            statement.setInt(1, message.getAfter().getStatus());
                            statement.setInt(2, message.getAfter().getUserID());
                            statement.setString(3, message.getAfter().getCurrency());
                            statement.setLong(4, message.getAfter().getId());
                            statement.setTimestamp(5, message.getAfter().getDate());
                            statement.setString(6, message.getAfter().getCoreTransactionID());

                            statement.addBatch();
                            statement.executeBatch();
                            System.out.println("Record with ID " + message.getAfter().getId() + " has been Inserted");
                        }
                    }else if(message.getAfter() != null && message.getBefore() != null){
                        if (message.getAfter().getProviderLabel() != null && (message.getAfter().getProviderLabel().equals("expresspay"))) {

                            statement2.setInt(1, message.getAfter().getStatus());
                            statement2.setInt(2, message.getAfter().getUserID());
                            statement2.setString(3, message.getAfter().getCurrency());
                            statement2.setTimestamp(4, message.getAfter().getDate());
                            statement2.setString(5, message.getAfter().getCoreTransactionID());
                            statement2.setLong(6, message.getAfter().getId());

                            statement2.executeUpdate();
                            System.out.println("Record with ID " + message.getAfter().getId() + " has been Updated!");
                        }
                    }
                    consumer.commitAsync();
                }
            }
        } catch(Exception e){
            logger.error("Unexpected error", e);
        } finally{
            try {
                consumer.commitSync();
            } finally {
                consumer.close();
            }
        }
    }
}
