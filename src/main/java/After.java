import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.processing.Generated;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ID",
        "ProviderID",
        "UserID",
        "UserName",
        "IP",
        "TransactionID",
        "CoreTransactionID",
        "ProviderTransactionID",
        "ProviderTrMd5",
        "ProviderTxReferenceID",
        "ProviderLabel",
        "ServiceID",
        "Amount",
        "Fee",
        "Currency",
        "TransactionType",
        "PaymentAccount",
        "Status",
        "PIN",
        "Date",
        "DecisionDate",
        "LastActivityDate",
        "AdditionalData"
})
@Generated("jsonschema2pojo")
@JsonDeserialize
public class After {

    @JsonProperty("ID")
    private long id;
    @JsonProperty("ProviderID")
    private String providerID;
    @JsonProperty("UserID")
    private Integer userID;
    @JsonProperty("UserName")
    private String userName;
    @JsonProperty("IP")
    private String ip;
    @JsonProperty("TransactionID")
    private String transactionID;
    @JsonProperty("CoreTransactionID")
    private String coreTransactionID;
    @JsonProperty("ProviderTransactionID")
    private String providerTransactionID;
    @JsonProperty("ProviderTrMd5")
    private String providerTrMd5;
    @JsonProperty("ProviderTxReferenceID")
    private String providerTxReferenceID;
    @JsonProperty("ProviderLabel")
    private String providerLabel;
    @JsonProperty("ServiceID")
    private Integer serviceID;
    @JsonProperty("Amount")
    private Integer amount;
    @JsonProperty("Fee")
    private Integer fee;
    @JsonProperty("Currency")
    private String currency;
    @JsonProperty("TransactionType")
    private String transactionType;
    @JsonProperty("PaymentAccount")
    private Object paymentAccount;
    @JsonProperty("Status")
    private Integer status;
    @JsonProperty("PIN")
    private String pin;
    @JsonProperty("Date")
    private String date;
    @JsonProperty("DecisionDate")
    private String decisionDate;
    @JsonProperty("LastActivityDate")
    private String lastActivityDate;
    @JsonProperty("AdditionalData")
    private String additionalData;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public long getId() {
        return id;
    }

    @JsonProperty("ID")
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty("ProviderID")
    public String getProviderID() {
        return providerID;
    }

    @JsonProperty("ProviderID")
    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    @JsonProperty("UserID")
    public Integer getUserID() {
        return userID;
    }

    @JsonProperty("UserID")
    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    @JsonProperty("UserName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("UserName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("IP")
    public String getIp() {
        final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
        if(ip != null) {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] bytes = decoder.decode(ip);
            char[] result = new char[bytes.length * 2];
            for(int j = 0; j < bytes.length; j++){
                int v = bytes[j] & 0xFF;
                result[j * 2] = HEX_ARRAY[v >>> 4];
                result[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
            }
            String finalResult = String.valueOf(result);
            return finalResult;
        }
        return null;
    }

    @JsonProperty("IP")
    public void setIp(String ip) {
        this.ip = ip;
    }

    @JsonProperty("TransactionID")
    public String getTransactionID() {
        return transactionID;
    }

    @JsonProperty("TransactionID")
    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @JsonProperty("CoreTransactionID")
    public String getCoreTransactionID() {
        final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
        if(coreTransactionID != null) {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] bytes = decoder.decode(coreTransactionID);
            char[] result = new char[bytes.length * 2];
            for(int j = 0; j < bytes.length; j++){
                int v = bytes[j] & 0xFF;
                result[j * 2] = HEX_ARRAY[v >>> 4];
                result[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
            }
            String finalResult = String.valueOf(result);
            return finalResult;
        }
        return null;
    }

    @JsonProperty("CoreTransactionID")
    public void setCoreTransactionID(String coreTransactionID) {
        this.coreTransactionID = coreTransactionID;
    }

    @JsonProperty("ProviderTransactionID")
    public String getProviderTransactionID() {
        return providerTransactionID;
    }

    @JsonProperty("ProviderTransactionID")
    public void setProviderTransactionID(String providerTransactionID) {
        this.providerTransactionID = providerTransactionID;
    }

    @JsonProperty("ProviderTrMd5")
    public String getProviderTrMd5() {
        return providerTrMd5;
    }

    @JsonProperty("ProviderTrMd5")
    public void setProviderTrMd5(String providerTrMd5) {
        this.providerTrMd5 = providerTrMd5;
    }

    @JsonProperty("ProviderTxReferenceID")
    public String getProviderTxReferenceID() {
        return providerTxReferenceID;
    }

    @JsonProperty("ProviderTxReferenceID")
    public void setProviderTxReferenceID(String providerTxReferenceID) {
        this.providerTxReferenceID = providerTxReferenceID;
    }

    @JsonProperty("ProviderLabel")
    public String getProviderLabel() {
        return providerLabel;
    }

    @JsonProperty("ProviderLabel")
    public void setProviderLabel(String providerLabel) {
        this.providerLabel = providerLabel;
    }

    @JsonProperty("ServiceID")
    public Integer getServiceID() {
        return serviceID;
    }

    @JsonProperty("ServiceID")
    public void setServiceID(Integer serviceID) {
        this.serviceID = serviceID;
    }

    @JsonProperty("Amount")
    public Integer getAmount() {
        return amount;
    }

    @JsonProperty("Amount")
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @JsonProperty("Fee")
    public Integer getFee() {
        return fee;
    }

    @JsonProperty("Fee")
    public void setFee(Integer fee) {
        this.fee = fee;
    }

    @JsonProperty("Currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("Currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("TransactionType")
    public String getTransactionType() {
        return transactionType;
    }

    @JsonProperty("TransactionType")
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @JsonProperty("PaymentAccount")
    public Object getPaymentAccount() {
        return paymentAccount;
    }

    @JsonProperty("PaymentAccount")
    public void setPaymentAccount(Object paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    @JsonProperty("Status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("PIN")
    public String getPin() {
        return pin;
    }

    @JsonProperty("PIN")
    public void setPin(String pin) {
        this.pin = pin;
    }

    @JsonProperty("Date")
    public Timestamp getDate() throws ParseException {
        ZoneId z = ZoneId.of("Asia/Tbilisi");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter).atZone(z).toLocalDateTime();
        Timestamp sqlDate = Timestamp.valueOf(dateTime);
        return sqlDate;
    }


    @JsonProperty("Date")
    public void setDate(String date) {
        this.date = date;
    }


    @JsonProperty("DecisionDate")
    public String getDecisionDate() {
        return decisionDate;
    }

    @JsonProperty("DecisionDate")
    public void setDecisionDate(String decisionDate) {
        this.decisionDate = decisionDate;
    }

    @JsonProperty("LastActivityDate")
    public String getLastActivityDate() {
        return lastActivityDate;
    }

    @JsonProperty("LastActivityDate")
    public void setLastActivityDate(String lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    @JsonProperty("AdditionalData")
    public String getAdditionalData() {
        return additionalData;
    }

    @JsonProperty("AdditionalData")
    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
